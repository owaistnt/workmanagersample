package com.planactreivew.workmanagersample
import android.annotation.SuppressLint
import android.arch.lifecycle.Observer
import android.content.Context
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.TextView
import android.widget.Toast
import androidx.work.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.concurrent.TimeUnit

class MainActivity : AppCompatActivity() {

    lateinit var textView:TextView;
    @SuppressLint("NewApi")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
         textView=findViewById<TextView>(R.id.txtHello);
        textView.text="Application Started ${getTime()}"


        enqueueNextTask(1)

        WorkManager.getInstance().getWorkInfosByTagLiveData("one").observe(this, Observer {
            it?.let {workinfos->
                val first=workinfos[0]
                setText("${ first.id }:  ${first.state}")
                when (first.state) {
                    WorkInfo.State.SUCCEEDED -> {
                        val time=first.outputData.getInt("time", 0)
                        if (time > 0) {
                           // enqueueNextTask(time.toLong())
                            setText("Scheduled Next Work to $time")
                        }
                    }
                }

            }
        })

    }

    private fun enqueueNextTask(timeInMinute: Long) {
        val oneTimeRequest = OneTimeWorkRequestBuilder<SimpleWorker>()
            .setInitialDelay(timeInMinute, TimeUnit.MINUTES)
            .addTag("one")
            .build()

        WorkManager.getInstance()
            .beginWith(oneTimeRequest)
            .enqueue()
    }

    @SuppressLint("NewApi")
    private fun setText(s: String) {
        textView.text =  "${textView.text} \nState: $s ${getTime()}"
    }


    @SuppressLint("NewApi")
    fun getTime(): String {
        val current = LocalDateTime.now()

        val formatter = DateTimeFormatter.ofPattern("HH:mm")
        val formatted = current.format(formatter) ?: ""
        return formatted
    }

    private fun directAPIHit() {
        RandomUserAPI(RetrofitProvider().getInstance()).getUsers(10).enqueue(object : Callback<Result> {
            override fun onFailure(call: Call<Result>, t: Throwable) {
                Toast.makeText(this@MainActivity, "Failed: $t", Toast.LENGTH_LONG).show()
                Log.e("APP", t.toString())
                findViewById<TextView>(R.id.txtHello).text = "Failed: $t"
            }

            @SuppressLint("SetTextI18n")
            override fun onResponse(call: Call<Result>, response: Response<Result>) {
                if (response.isSuccessful) {
                    val size = response.body()?.results?.size
                    findViewById<TextView>(R.id.txtHello).text = "Success: $size"
                } else {
                    findViewById<TextView>(R.id.txtHello).text = "Success: ${response.code()}"

                }
            }

        })
    }
}



class SimpleWorker(val context: Context, val workParams: WorkerParameters) : Worker(context, workParams) {
    override fun doWork(): Result {
        val execute = RandomUserAPI(RetrofitProvider().getInstance()).getUsers(10).execute()
        return if (execute.isSuccessful) {
            Result.success(workDataOf("time" to 2))
        }else{
            Result.failure()
        }
    }

}
