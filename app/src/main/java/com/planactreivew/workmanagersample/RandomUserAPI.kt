package com.planactreivew.workmanagersample

import okhttp3.OkHttpClient
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query


data class User(val gender: String, val name: Name)
data class Name(val title: String, val first: String, val last: String)
data class Result(val results: List<User>)

class RetrofitProvider{

    fun getInstance() = Retrofit.Builder()
        .baseUrl("https://randomuser.me")
        .addConverterFactory(GsonConverterFactory.create())
        .client(OkHttpClient())
        .build()

}

class RandomUserAPI(private val retrofit: Retrofit) {

    private val api by lazy {
        retrofit.create(IRandomUserAPI::class.java)
    }

    fun getUsers(count: Int): Call<Result> {
       return api.listRepos(count.toString())
    }

    interface IRandomUserAPI {

        @GET("/api/")
        fun listRepos(@Query("results") result: String): Call<Result>

    }
}

